package com.tpKotlin.glhf.tpkotlin.tools

import java.io.File
import java.io.FileOutputStream

object FileManager {
	val FILE_NAME = "monTonton.sappellePasRichard"
	val PATH_TO_FILE = "src/main/resources/files/"

	fun thereIsSomethingInTheFile(): Boolean {
		var res = false
		File(PATH_TO_FILE + FILE_NAME).bufferedReader().lines().use { lines ->
			for (line in lines) {
				if (!line.startsWith('#')) {
					res = true
					break
				}
			}
		}

		return res
	}

	fun loadAllFromFile() {
		//charger les images depuis le fichier vers le singleton
	}

	fun saveMemeInFile(name: String, url: String) {
		//sauvegarder une image dans le fichier
	}
}